package dynamic.log.core;

import dynamic.log.core.common.ApplicationContextHolder;
import dynamic.log.core.common.env.DLogProperties;
import dynamic.log.core.common.log.Slf4jLogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;

/**
 *
 * @author qp
 * @date 2023/4/15 23:03
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(DLogProperties.class)
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class DLogBaseBeanConfiguration {

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public ApplicationContextHolder applicationContextHolder() {
        return new ApplicationContextHolder();
    }

    @Bean
    public DLogRegistry dtpRegistry() {
        return new DLogRegistry();
    }


    @Bean(initMethod = "init")
    @ConditionalOnMissingBean
    public Slf4jLogFactory slf4jLogFactory() {
        return new Slf4jLogFactory();
    }
}
