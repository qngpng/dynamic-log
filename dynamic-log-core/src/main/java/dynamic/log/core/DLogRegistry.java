package dynamic.log.core;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ServiceLoaderUtil;
import cn.hutool.core.util.StrUtil;
import dynamic.log.core.common.ApplicationContextHolder;
import dynamic.log.core.common.env.DLogProperties;
import dynamic.log.core.common.env.SimpleLogProperties;
import dynamic.log.core.common.env.constant.DynamicLogConst;
import dynamic.log.core.common.log.DynamicLogFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.logging.LogLevel;
import org.springframework.boot.logging.LoggerConfiguration;
import org.springframework.boot.logging.LoggingSystem;
import org.springframework.core.Ordered;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 配置注册
 *
 * @author qp
 * @date 2023/4/16 14:48
 */
@Slf4j
public class DLogRegistry implements ApplicationRunner, Ordered {

    private static DLogProperties dLogProperties;

    private static LoggingSystem loggingSystem;


    public static void refresh(DLogProperties dLogProperties) {
        String applicationName = ApplicationContextHolder.getServerName();
        List<SimpleLogProperties> dLog = dLogProperties.getDLog();
        if (CollUtil.isEmpty(dLog)) {
            return;
        }
        for (SimpleLogProperties simpleLogProperties : dLog) {
            String level = simpleLogProperties.getLevel();
            String packageName = simpleLogProperties.getPackageName();
            String serviceName = simpleLogProperties.getServiceName();
            if (!StrUtil.equalsIgnoreCase(applicationName, serviceName)) {
                continue;
            }
            setDynamicLevel(packageName, level);
        }
    }

    /**
     * 设置日志级别(hutool)
     *
     * @param packageName 包名
     * @param level       日志等级
     */
    public static void refreshLogLevel(String packageName, String level) {
        packageName = DynamicLogConst.STAR.equals(packageName) ? LoggingSystem.ROOT_LOGGER_NAME : packageName;
        final DynamicLogFactory factory = ServiceLoaderUtil.loadFirstAvailable(DynamicLogFactory.class);
        if (factory != null) {
            factory.refreshLogLevel(packageName, level);
        }
    }

    /**
     * 设置日志级别(springboot)
     *
     * @param packageName 包名
     * @param level       日志等级
     */
    public static void setDynamicLevel(String packageName, String level) {
        packageName = DynamicLogConst.STAR.equals(packageName) ? Strings.EMPTY : packageName;
        level = level.toUpperCase();
        Set<String> supportLevels = loggingSystem.getSupportedLogLevels().stream().map(Enum::name).collect(Collectors.toSet());
        if (!supportLevels.contains(level)) {
            log.warn("[动态日志]不支持配置的[{}]日志级别", level);
            return;
        }
        LoggerConfiguration loggerConfiguration = loggingSystem.getLoggerConfiguration(packageName);
        if (loggerConfiguration == null) {
            log.warn("[动态日志]请检查包[{}]是否正确", packageName);
            return;
        }
        if (!loggerConfiguration.getEffectiveLevel().name().equalsIgnoreCase(level)) {
            loggingSystem.setLogLevel(packageName, LogLevel.valueOf(level));
        }
    }

    @Autowired
    public void setDLogProperties(DLogProperties dLogProperties) {
        DLogRegistry.dLogProperties = dLogProperties;
    }

    @Resource
    public void setLoggingSystem(LoggingSystem loggingSystem) {
        DLogRegistry.loggingSystem = loggingSystem;
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 1;
    }

    @Override
    public void run(ApplicationArguments args) {
        // 根据配置文件刷新日志级别
        refresh(dLogProperties);

        String applicationName = ApplicationContextHolder.getServerName();
        dLogProperties.getDLog()
                .stream()
                .filter(f -> StrUtil.equalsIgnoreCase(applicationName, f.getServiceName()))
                .forEach(dlog -> log.debug("DLogRegistry has been initialized, packageName: {}, level: {}",
                        dlog.getPackageName(), dlog.getLevel()));
    }
}
