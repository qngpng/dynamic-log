package dynamic.log.core.common.env;

import cn.hutool.core.collection.ListUtil;
import dynamic.log.core.common.env.constant.DynamicLogConst;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;


/**
 * 配置
 *
 * @author qp
 * @date 2023/4/15 10:25
 */
@Data
@ConfigurationProperties(prefix = DynamicLogConst.MAIN_PROPERTIES_PREFIX)
public class DLogProperties {

    /**
     * nacos配置信息
     * 主要为了适配boot版nacos
     */
    private Nacos nacos;

    /**
     * 配置类型
     */
    private String configType = "yml";

    /**
     * 配置详细
     */
    private List<SimpleLogProperties> dLog = ListUtil.list(false);


    @Data
    public static class Nacos {

        private String dataId;

        private String group;

        private String namespace;
    }
}
