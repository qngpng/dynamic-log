package dynamic.log.core.common.env;

import dynamic.log.core.common.env.constant.DynamicLogConst;
import dynamic.log.core.common.env.enums.LogLevelEnum;
import lombok.Data;

/**
 * 主要属性
 *
 * @author qp
 * @date 2023/4/15 10:29
 */
@Data
public class SimpleLogProperties {

    /**
     * 服务名
     */
    private String serviceName;

    /**
     * 包名
     */
    private String packageName = DynamicLogConst.STAR;

    /**
     * 日志级别
     */
    private String level = LogLevelEnum.INFO.name();

}
