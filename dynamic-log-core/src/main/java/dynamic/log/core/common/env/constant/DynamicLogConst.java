package dynamic.log.core.common.env.constant;

/**
 * 常量
 *
 * @author qp
 * @date 2023/4/15 10:40
 */
public interface DynamicLogConst {

    /**
     * 主配置名称
     */
    String MAIN_PROPERTIES_PREFIX = "spring.dynamic.log";

    /**
     * 开关
     */
    String ENABLED = MAIN_PROPERTIES_PREFIX + ".enabled";

    /**
     * symbol
     */
    String DOT = ".";

    String STAR = "*";

    String ARR_LEFT_BRACKET = "[";

    String ARR_RIGHT_BRACKET = "]";


    /**
     * log4j的sl4j2的实现工厂
     */
    String LOG4J_SLF4J_FACTORY = "org.slf4j.impl.Reload4jLoggerFactory";

    /**
     * logback的sl4j2的实现工厂
     */
    String LOGBACK_SLF4J_FACTORY = "ch.qos.logback.classic.util.ContextSelectorStaticBinder";

    /**
     * log4j2的sl4j2的实现工厂
     */
    String LOG4J2_SLF4J_FACTORY = "org.apache.logging.slf4j.Log4jLoggerFactory";
}
