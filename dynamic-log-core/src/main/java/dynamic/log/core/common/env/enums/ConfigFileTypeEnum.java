package dynamic.log.core.common.env.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author qp
 * @date 2023/4/15 11:12
 */
@Getter
@AllArgsConstructor
public enum ConfigFileTypeEnum {
    /**
     * 配置文件类型
     */
    PROPERTIES("properties"),
    XML("xml"),
    JSON("json"),
    YML("yml"),
    YAML("yaml"),
    TXT("txt");

    private final String value;

    public static ConfigFileTypeEnum of(String value) {
        for (ConfigFileTypeEnum typeEnum : ConfigFileTypeEnum.values()) {
            if (typeEnum.value.equals(value)) {
                return typeEnum;
            }
        }
        return PROPERTIES;
    }
}
