package dynamic.log.core.common.env.enums;

/**
 * 日志级别
 *
 * @author qp
 * @date 2023/4/15 10:35
 */
public enum LogLevelEnum {
    /**
     * 默认日志级别
     */
    INFO,

    ALL,

    TRACE,

    DEBUG,

    WARN,

    ERROR,

    FATAL,

    OFF,
}
