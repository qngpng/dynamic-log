package dynamic.log.core.common.env.enums;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.slf4j.impl.StaticLoggerBinder;

/**
 * 支持的日志框架类型
 *
 * @author qp
 * @date 2023/9/15 16:47
 */
@Slf4j
public enum LogTypeEnum {

    LOG4J {
        @Override
        public void refreshLogLevel(String packageName, String logLevel) {
            org.apache.log4j.Logger logger = org.apache.log4j.LogManager.getLogger(packageName);
            org.apache.log4j.Level level = logger.getLevel();
            org.apache.log4j.Level newLevel = org.apache.log4j.Level.toLevel(logLevel, level);
            logger.setLevel(newLevel);
        }
    },

    LOGBACK {
        @Override
        public void refreshLogLevel(String packageName, String logLevel) {
            ch.qos.logback.classic.LoggerContext loggerContext =
                    (ch.qos.logback.classic.LoggerContext) StaticLoggerBinder.getSingleton().getLoggerFactory();
            ch.qos.logback.classic.Logger logger = loggerContext.getLogger(packageName);
            ch.qos.logback.classic.Level level = logger.getLevel();
            // level值不正确，使用原level
            ch.qos.logback.classic.Level newLevel = ch.qos.logback.classic.Level.toLevel(logLevel, level);
            logger.setLevel(newLevel);
        }
    },

    LOG4J2 {
        @Override
        public void refreshLogLevel(String packageName, String logLevel) {
            org.apache.logging.log4j.core.LoggerContext loggerContext =
                    org.apache.logging.log4j.core.LoggerContext.getContext(false);
            org.apache.logging.log4j.core.config.Configuration configuration = loggerContext.getConfiguration();
            if (loggerContext.hasLogger(packageName)) {
                // 已经存在该logger
                org.apache.logging.log4j.core.Logger logger = loggerContext.getLogger(packageName);
                org.apache.logging.log4j.core.config.LoggerConfig loggerConfig = configuration.getLoggerConfig(logger.getName());
                org.apache.logging.log4j.Level oldLevel = logger.getLevel();
                org.apache.logging.log4j.Level level = org.apache.logging.log4j.Level.toLevel(logLevel, oldLevel);
                loggerConfig.setLevel(level);
            }
            else {
                Level level = Level.toLevel(logLevel);
                LoggerConfig loggerConfig = new LoggerConfig.Builder<>()
                        .withLevel(level)
                        .withLoggerName(packageName)
                        .withConfig(configuration)
                        .build();
                configuration.addLogger(packageName, loggerConfig);
            }
            // This causes all Loggers to refresh information from their LoggerConfig.
            loggerContext.updateLoggers();

            //ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();
            //ConfigurationSource source = new ConfigurationSource(new ByteArrayInputStream(content.getBytes()));
            //builder.setStatusLevel(level);
            //builder.setConfigurationSource(source);
            //builder.setConfigurationName(configuration.getName());
            //builder.setPackages(configuration.getClass().getPackage().getName());
            //BuiltConfiguration newConfiguration = builder.build();
            //loggerContext.start(newConfiguration);
            //Configurator.initialize(newConfiguration);
        }
    },

    NOT_SUPPORTED {
        @Override
        public void refreshLogLevel(String packageName, String logLevel) {
            log.warn("项目使用的日志框架未支持，无法更新");
        }
    },

    ;

    public abstract void refreshLogLevel(String packageName, String logLevel);
}
