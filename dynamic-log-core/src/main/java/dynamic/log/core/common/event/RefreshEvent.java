package dynamic.log.core.common.event;

import dynamic.log.core.common.env.DLogProperties;
import org.springframework.context.ApplicationEvent;

/**
 * RefreshEvent related
 *
 * @author yanhom
 * @since 1.0.0
 */
public class RefreshEvent extends ApplicationEvent {

    private final transient DLogProperties dtpProperties;

    public RefreshEvent(Object source, DLogProperties dtpProperties) {
        super(source);
        this.dtpProperties = dtpProperties;
    }

    public DLogProperties getDtpProperties() {
        return dtpProperties;
    }
}
