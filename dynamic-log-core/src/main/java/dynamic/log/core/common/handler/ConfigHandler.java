package dynamic.log.core.common.handler;

import cn.hutool.core.collection.ListUtil;
import dynamic.log.core.common.env.enums.ConfigFileTypeEnum;
import dynamic.log.core.common.parser.PropertiesConfigParser;
import dynamic.log.core.common.parser.ConfigParser;
import dynamic.log.core.common.parser.JsonConfigParser;
import dynamic.log.core.common.parser.YamlConfigParser;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * @author qp
 * @date 2023/4/15 11:18
 */
public class ConfigHandler {
    private static final List<ConfigParser> PARSERS = ListUtil.list(false);

    private ConfigHandler() {
        ServiceLoader<ConfigParser> loader = ServiceLoader.load(ConfigParser.class);
        for (ConfigParser configParser : loader) {
            PARSERS.add(configParser);
        }

        PARSERS.add(new PropertiesConfigParser());
        PARSERS.add(new YamlConfigParser());
        PARSERS.add(new JsonConfigParser());
    }

    public Map<Object, Object> parseConfig(String content, ConfigFileTypeEnum type) throws IOException {
        for (ConfigParser parser : PARSERS) {
            if (parser.supports(type)) {
                return parser.doParse(content);
            }
        }

        return Collections.emptyMap();
    }

    public static ConfigHandler getInstance() {
        return ConfigHandlerHolder.INSTANCE;
    }

    private static class ConfigHandlerHolder {
        private static final ConfigHandler INSTANCE = new ConfigHandler();
    }
}
