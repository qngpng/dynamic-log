package dynamic.log.core.common.log;

import cn.hutool.log.LogFactory;
import lombok.extern.slf4j.Slf4j;

/**
 * 动态日志
 * 使用SPI机制获取项目使用的日志框架
 * <p>
 * 扩展{@link LogFactory}实现动态刷新日志
 *
 * @author qp
 * @date 2023/4/22 22:37
 */
@Slf4j
public abstract class DynamicLogFactory extends LogFactory {

    public DynamicLogFactory(String name) {
        super(name);
    }

    public void refreshLogLevel(String packageName, String logLevel) {
        throw new UnsupportedOperationException("Unable to set log level");
    }
}
