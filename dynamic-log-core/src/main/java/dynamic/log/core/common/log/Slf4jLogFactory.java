package dynamic.log.core.common.log;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import dynamic.log.core.common.ApplicationContextHolder;
import dynamic.log.core.common.env.DLogProperties;
import dynamic.log.core.common.env.SimpleLogProperties;
import dynamic.log.core.common.env.constant.DynamicLogConst;
import dynamic.log.core.common.env.enums.LogTypeEnum;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.StaticLoggerBinder;

import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 日志工厂
 * <p>
 * 使用slf4j的{@link StaticLoggerBinder#getLoggerFactoryClassStr()}获取当前使用的日志类型
 *
 * @author qp
 * @date 2023/9/15 16:41
 */
@Slf4j
public class Slf4jLogFactory {

    /**
     * 缓存logger
     */
    private final Map<String, Object> loggerMap = new ConcurrentHashMap<>();

    /**
     * 项目使用的日志类型
     */
    private LogTypeEnum logTypeEnum;

    public void init() {
        String type = StaticLoggerBinder.getSingleton().getLoggerFactoryClassStr();
        if (DynamicLogConst.LOG4J_SLF4J_FACTORY.equals(type)) {
            logTypeEnum = LogTypeEnum.LOG4J;

            Enumeration<?> enumeration = org.apache.log4j.LogManager.getCurrentLoggers();
            while (enumeration.hasMoreElements()) {
                org.apache.log4j.Logger logger = (org.apache.log4j.Logger) enumeration.nextElement();
                if (logger.getLevel() != null) {
                    loggerMap.put(logger.getName(), logger);
                }
            }
            org.apache.log4j.Logger rootLogger = org.apache.log4j.LogManager.getRootLogger();
            loggerMap.put(rootLogger.getName(), rootLogger);
        }
        else if (DynamicLogConst.LOGBACK_SLF4J_FACTORY.equals(type)) {
            logTypeEnum = LogTypeEnum.LOGBACK;

            ch.qos.logback.classic.LoggerContext loggerContext = (ch.qos.logback.classic.LoggerContext) LoggerFactory.getILoggerFactory();
            for (ch.qos.logback.classic.Logger logger : loggerContext.getLoggerList()) {
                if (logger.getLevel() != null) {
                    loggerMap.put(logger.getName(), logger);
                }
            }
            ch.qos.logback.classic.Logger rootLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
            loggerMap.put(rootLogger.getName(), rootLogger);
        }
        else if (DynamicLogConst.LOG4J2_SLF4J_FACTORY.equals(type)) {
            logTypeEnum = LogTypeEnum.LOG4J2;

            org.apache.logging.log4j.core.LoggerContext loggerContext = (org.apache.logging.log4j.core.LoggerContext) org.apache.logging.log4j.LogManager.getContext(false);
            Map<String, LoggerConfig> map = loggerContext.getConfiguration().getLoggers();
            for (org.apache.logging.log4j.core.config.LoggerConfig loggerConfig : map.values()) {
                String key = loggerConfig.getName();
                if (StrUtil.isBlank(key)) {
                    key = "root";
                }
                loggerMap.put(key, loggerConfig);
            }
        }
        else {
            logTypeEnum = LogTypeEnum.NOT_SUPPORTED;
            log.warn("未支持项目使用的日志框架：[{}]", type);
        }
    }

    /**
     * 获取项目日志配置
     */
    public JSONObject getLoggers() {
        JSONObject result = new JSONObject();
        result.put("logType", logTypeEnum);
        JSONArray loggerList = new JSONArray();
        for (ConcurrentMap.Entry<String, Object> entry : loggerMap.entrySet()) {
            JSONObject loggerJson = new JSONObject();
            loggerJson.put("logName", entry.getKey());
            if (logTypeEnum == LogTypeEnum.LOG4J) {
                org.apache.log4j.Logger targetLogger = (org.apache.log4j.Logger) entry.getValue();
                loggerJson.put("logLevel", targetLogger.getLevel().toString());
            }
            else if (logTypeEnum == LogTypeEnum.LOGBACK) {
                ch.qos.logback.classic.Logger targetLogger = (ch.qos.logback.classic.Logger) entry.getValue();
                loggerJson.put("logLevel", targetLogger.getLevel().toString());
            }
            else if (logTypeEnum == LogTypeEnum.LOG4J2) {
                org.apache.logging.log4j.core.config.LoggerConfig targetLogger = (org.apache.logging.log4j.core.config.LoggerConfig) entry.getValue();
                loggerJson.put("logLevel", targetLogger.getLevel().toString());
            }
            loggerList.add(loggerJson);
        }
        result.put("loggers", loggerList);
        return result;
    }

    public void refresh(DLogProperties dtpProperties) {
        String applicationName = ApplicationContextHolder.getServerName();
        List<SimpleLogProperties> dLog = dtpProperties.getDLog();
        if (CollUtil.isEmpty(dLog)) {
            return;
        }
        for (SimpleLogProperties simpleLogProperties : dLog) {
            String logLevel = simpleLogProperties.getLevel();
            String packageName = simpleLogProperties.getPackageName();
            String serviceName = simpleLogProperties.getServiceName();
            if (!StrUtil.equalsIgnoreCase(applicationName, serviceName)) {
                continue;
            }
            logTypeEnum.refreshLogLevel(packageName, logLevel);
        }
    }

}
