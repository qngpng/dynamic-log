package dynamic.log.core.common.log.log4j;

import cn.hutool.log.Log;
import cn.hutool.log.dialect.log4j.Log4jLog;
import dynamic.log.core.common.env.enums.LogTypeEnum;
import dynamic.log.core.common.log.DynamicLogFactory;
import com.google.auto.service.AutoService;
import lombok.extern.slf4j.Slf4j;

/**
 * log4j日志工厂
 *
 * @author qp
 * @date 2023/9/16 20:02
 */
@Slf4j
@AutoService(DynamicLogFactory.class)
public class DynamicLog4jFactory extends DynamicLogFactory {

    public DynamicLog4jFactory() {
        super("Log4j");
        checkLogExist(org.apache.log4j.Logger.class);
    }

    @Override
    public Log createLog(String name) {
        return new Log4jLog(name);
    }

    @Override
    public Log createLog(Class<?> clazz) {
        return new Log4jLog(clazz);
    }

    @Override
    public void refreshLogLevel(String packageName, String logLevel) {
        LogTypeEnum.LOG4J.refreshLogLevel(packageName, logLevel);
    }
}
