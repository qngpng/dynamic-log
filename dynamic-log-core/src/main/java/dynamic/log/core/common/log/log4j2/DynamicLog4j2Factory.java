package dynamic.log.core.common.log.log4j2;

import cn.hutool.log.Log;
import cn.hutool.log.dialect.log4j2.Log4j2Log;
import dynamic.log.core.common.env.enums.LogTypeEnum;
import dynamic.log.core.common.log.DynamicLogFactory;
import com.google.auto.service.AutoService;
import lombok.extern.slf4j.Slf4j;

/**
 * log4j2的动态日志工厂
 *
 * @author qp
 * @date 2023/4/22 21:33
 */
@Slf4j
@AutoService(DynamicLogFactory.class)
public class DynamicLog4j2Factory extends DynamicLogFactory {

    public DynamicLog4j2Factory() {
        super("dynamicLog4j2");
        checkLogExist(org.apache.logging.log4j.LogManager.class);
    }

    @Override
    public Log createLog(String name) {
        return new Log4j2Log(name);
    }

    @Override
    public Log createLog(Class<?> clazz) {
        return new Log4j2Log(clazz);
    }

    @Override
    public void refreshLogLevel(String packageName, String logLevel){
        LogTypeEnum.LOG4J2.refreshLogLevel(packageName, logLevel);
    }
}
