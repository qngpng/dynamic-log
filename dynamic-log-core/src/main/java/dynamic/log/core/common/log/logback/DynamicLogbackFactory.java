package dynamic.log.core.common.log.logback;

import cn.hutool.log.Log;
import cn.hutool.log.dialect.slf4j.Slf4jLog;
import dynamic.log.core.common.env.enums.LogTypeEnum;
import dynamic.log.core.common.log.DynamicLogFactory;
import com.google.auto.service.AutoService;
import lombok.extern.slf4j.Slf4j;

/**
 * logback的动态日志工厂
 *
 * @author qp
 * @date 2023/4/22 22:21
 */
@Slf4j
@AutoService(DynamicLogFactory.class)
public class DynamicLogbackFactory extends DynamicLogFactory {

    public DynamicLogbackFactory() {
        super("dynamicLogback");
        checkLogExist(ch.qos.logback.classic.LoggerContext.class);
    }

    @Override
    public Log createLog(String name) {
        return new Slf4jLog(name);
    }

    @Override
    public Log createLog(Class<?> clazz) {
        return new Slf4jLog(clazz);
    }

    @Override
    public void refreshLogLevel(String packageName, String logLevel) {
        LogTypeEnum.LOGBACK.refreshLogLevel(packageName, logLevel);
    }
}
