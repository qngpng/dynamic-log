package dynamic.log.core.common.parser;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import dynamic.log.core.common.env.constant.DynamicLogConst;
import dynamic.log.core.common.env.enums.ConfigFileTypeEnum;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.*;



/**
 * JsonConfigParser related
 *
 * @author yanhom
 * @since 1.0.5
 **/
@Slf4j
@SuppressWarnings("unchecked")
public class JsonConfigParser extends AbstractConfigParser {

    private static final List<ConfigFileTypeEnum> CONFIG_TYPES = ListUtil.of(ConfigFileTypeEnum.JSON);

    @Override
    public List<ConfigFileTypeEnum> types() {
        return CONFIG_TYPES;
    }

    @Override
    public Map<Object, Object> doParse(String content) throws IOException {
        if (StrUtil.isEmpty(content)) {
            return Collections.emptyMap();
        }
        return doParse(content, DynamicLogConst.MAIN_PROPERTIES_PREFIX);
    }

    @Override
    public Map<Object, Object> doParse(String content, String prefix) {

        Map<String, Object> originMap = JSON.parseObject(content, LinkedHashMap.class);
        Map<Object, Object> result = MapUtil.newHashMap();

        flatMap(result, originMap, prefix);
        return result;
    }

    private void flatMap(Map<Object, Object> result, Map<String, Object> dataMap, String prefix) {

        if (MapUtil.isEmpty(dataMap)) {
            return;
        }

        dataMap.forEach((k, v) -> {
            String fullKey = genFullKey(prefix, k);
            if (v instanceof Map) {
                flatMap(result, (Map<String, Object>) v, fullKey);
                return;
            } else if (v instanceof Collection) {
                int count = 0;
                for (Object obj : (Collection<Object>) v) {
                    String kk = DynamicLogConst.ARR_LEFT_BRACKET + (count++) + DynamicLogConst.ARR_RIGHT_BRACKET;
                    flatMap(result, Collections.singletonMap(kk, obj), fullKey);
                }
                return;
            }

            result.put(fullKey, v);
        });
    }

    private String genFullKey(String prefix, String key) {
        if (StrUtil.isEmpty(prefix)) {
            return key;
        }

        return key.startsWith(DynamicLogConst.ARR_LEFT_BRACKET) ? prefix.concat(key) : prefix.concat(DynamicLogConst.DOT).concat(key);
    }
}
