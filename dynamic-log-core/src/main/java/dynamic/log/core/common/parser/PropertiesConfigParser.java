package dynamic.log.core.common.parser;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.StrUtil;
import dynamic.log.core.common.env.enums.ConfigFileTypeEnum;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * PropertiesConfigParser related
 *
 * @author yanhom
 * @since 1.0.0
 **/
public class PropertiesConfigParser extends AbstractConfigParser {

    private static final List<ConfigFileTypeEnum> CONFIG_TYPES = ListUtil.of(ConfigFileTypeEnum.PROPERTIES);

    @Override
    public List<ConfigFileTypeEnum> types() {
        return CONFIG_TYPES;
    }

    @Override
    public Map<Object, Object> doParse(String content) throws IOException {

        if (StrUtil.isBlank(content)) {
            return Collections.emptyMap();
        }
        Properties properties = new Properties();
        properties.load(new StringReader(content));
        return properties;
    }
}
