package dynamic.log.core.common.refresh;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import dynamic.log.core.DLogRegistry;
import dynamic.log.core.common.ApplicationContextHolder;
import dynamic.log.core.common.env.DLogProperties;
import dynamic.log.core.common.env.enums.ConfigFileTypeEnum;
import dynamic.log.core.common.event.RefreshEvent;
import dynamic.log.core.common.handler.ConfigHandler;
import dynamic.log.core.common.spring.PropertiesBinder;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

/**
 * @author qp
 * @date 2023/4/15 11:14
 */
@Slf4j
public abstract class AbstractRefresher implements Refresher {

    @Resource
    protected DLogProperties dLogProperties;

    @Override
    public void refresh(String content, ConfigFileTypeEnum fileType) {
        if (StrUtil.isBlank(content) || Objects.isNull(fileType)) {
            log.warn("DynamicLog refresh, empty content or null fileType.");
            return;
        }

        try {
            val configHandler = ConfigHandler.getInstance();
            val properties = configHandler.parseConfig(content, fileType);
            doRefresh(properties);
        } catch (IOException e) {
            log.error("DynamicLog refresh error, content: {}, fileType: {}", content, fileType, e);
        }
    }

    protected void doRefresh(Map<Object, Object> properties) {
        if (MapUtil.isEmpty(properties)) {
            log.warn("DynamicLog refresh, empty properties.");
            return;
        }
        PropertiesBinder.bindDLogProperties(properties, dLogProperties);
        doRefresh(dLogProperties);
    }

    protected void doRefresh(DLogProperties dLogProperties) {
        DLogRegistry.refresh(dLogProperties);
        // 推送
        publishEvent(dLogProperties);
    }

    private void publishEvent(DLogProperties dLogProperties) {
        RefreshEvent event = new RefreshEvent(this, dLogProperties);
        ApplicationContextHolder.publishEvent(event);
    }
}
