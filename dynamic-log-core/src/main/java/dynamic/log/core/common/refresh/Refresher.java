package dynamic.log.core.common.refresh;

import dynamic.log.core.common.env.enums.ConfigFileTypeEnum;

/**
 *
 * @author qp
 * @date 2023/4/15 11:12
 */
public interface Refresher {
    /**
     * 刷新配置
     *
     * @param content 配置内容
     * @param fileType 文件类型
     */
    void refresh(String content, ConfigFileTypeEnum fileType);
}
