package dynamic.log.core.common.spring;

import dynamic.log.core.DLogBaseBeanConfiguration;
import dynamic.log.core.common.env.constant.DynamicLogConst;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.DeferredImportSelector;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;


/**
 * DtpConfigurationSelector related
 *
 * @author KamTo Hung
 * @since 1.1.1
 */
@Deprecated
public class DLogConfigurationSelector implements DeferredImportSelector, Ordered, EnvironmentAware {

    private Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public String[] selectImports(AnnotationMetadata metadata) {
        if (!environment.getProperty(DynamicLogConst.ENABLED, Boolean.class,Boolean.TRUE)) {
            return new String[]{};
        }
        return new String[]{DLogBaseBeanConfiguration.class.getName()};
    }

    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE;
    }

}
