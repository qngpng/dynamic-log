package dynamic.log.core.common.spring;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * EnableDynamicLog related
 *
 * @author yanhom
 * @since 1.0.4
 **/
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(DLogConfigurationSelector.class)
@Deprecated
public @interface EnableDynamicLog {
}
