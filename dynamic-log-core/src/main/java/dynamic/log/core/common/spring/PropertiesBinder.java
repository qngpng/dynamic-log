package dynamic.log.core.common.spring;

import dynamic.log.core.common.env.DLogProperties;
import dynamic.log.core.common.env.constant.DynamicLogConst;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
import org.springframework.core.ResolvableType;
import org.springframework.core.env.Environment;

import java.util.Map;


/**
 * PropertiesBinder related
 *
 * @author yanhom
 * @since 1.0.3
 **/
public class PropertiesBinder {

    private PropertiesBinder() {
    }

    /**
     * 绑定配置
     */
    public static void bindDLogProperties(Map<?, Object> properties, DLogProperties dLogProperties) {
        ConfigurationPropertySource sources = new MapConfigurationPropertySource(properties);
        Binder binder = new Binder(sources);
        ResolvableType type = ResolvableType.forClass(DLogProperties.class);
        Bindable<?> target = Bindable.of(type).withExistingValue(dLogProperties);
        binder.bind(DynamicLogConst.MAIN_PROPERTIES_PREFIX, target);
    }

    /**
     * 绑定配置
     */
    public static void bindDLogProperties(Environment environment, DLogProperties dLogProperties) {
        Binder binder = Binder.get(environment);
        ResolvableType type = ResolvableType.forClass(DLogProperties.class);
        Bindable<?> target = Bindable.of(type).withExistingValue(dLogProperties);
        binder.bind(DynamicLogConst.MAIN_PROPERTIES_PREFIX, target);
    }
}
