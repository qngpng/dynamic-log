package dynamic.log.core.common.utils;

import cn.hutool.core.util.StrUtil;
import dynamic.log.core.common.env.DLogProperties;
import dynamic.log.core.common.env.enums.ConfigFileTypeEnum;
import org.springframework.core.env.Environment;

/**
 * NacosUtil related
 *
 * @author yanhom
 * @since 1.0.0
 */
public final class NacosUtil {

    private NacosUtil() {}

    public static String deduceDataId(DLogProperties.Nacos nacos,
                                      Environment environment,
                                      ConfigFileTypeEnum configFileType) {
        String dataId;
        if (nacos != null && StrUtil.isNotBlank(nacos.getDataId())) {
            dataId = nacos.getDataId();
        } else {
            String[] profiles = environment.getActiveProfiles();
            if (profiles.length < 1) {
                profiles = environment.getDefaultProfiles();
            }

            String appName = environment.getProperty("spring.application.name");
            appName = StrUtil.isNotBlank(appName) ? appName : "application";

            // default dataId style, for example: demo-dev
            dataId = appName + "-" + profiles[0] + "." + configFileType.getValue();
        }

        return dataId;
    }

    public static String getGroup(DLogProperties.Nacos nacos, String defaultGroup) {
        String group = defaultGroup;
        if (nacos != null && StrUtil.isNotBlank(nacos.getGroup())) {
            group = nacos.getGroup();
        }
        return group;
    }

    public static ConfigFileTypeEnum getConfigType(DLogProperties dtpProperties,
                                                   ConfigFileTypeEnum defaultType) {
        ConfigFileTypeEnum configFileType = defaultType;
        if (StrUtil.isNotBlank(dtpProperties.getConfigType())) {
            configFileType = ConfigFileTypeEnum.of(dtpProperties.getConfigType());
        }
        return configFileType;
    }
}
