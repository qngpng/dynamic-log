package dynamic.log.boot.nacos.autoconfigure;

import dynamic.log.boot.nacos.refresh.NacosRefresher;
import dynamic.log.core.DLogBaseBeanConfiguration;
import com.alibaba.nacos.api.config.ConfigService;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动配置
 *
 * @author qp
 * @date 2023/5/1 10:26
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(ConfigService.class)
@ImportAutoConfiguration(DLogBaseBeanConfiguration.class)
public class DLogAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean()
    public NacosRefresher nacosRefresher() {
        return new NacosRefresher();
    }

}
