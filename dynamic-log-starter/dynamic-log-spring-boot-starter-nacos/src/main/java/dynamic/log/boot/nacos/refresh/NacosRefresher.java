package dynamic.log.boot.nacos.refresh;

import dynamic.log.core.common.ApplicationContextHolder;
import dynamic.log.core.common.env.DLogProperties;
import dynamic.log.core.common.env.enums.ConfigFileTypeEnum;
import dynamic.log.core.common.refresh.AbstractRefresher;
import dynamic.log.core.common.utils.NacosUtil;
import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;

import java.util.concurrent.Executor;

/**
 * @author qp
 * @date 2023/5/1 10:28
 */
@Slf4j
public class NacosRefresher extends AbstractRefresher implements InitializingBean, Listener {

    private ConfigFileTypeEnum configFileType;

    @NacosInjected
    private ConfigService configService;

    @Override
    public void afterPropertiesSet() {

        DLogProperties.Nacos nacos = dLogProperties.getNacos();
        configFileType = NacosUtil.getConfigType(dLogProperties, ConfigFileTypeEnum.PROPERTIES);
        String dataId = NacosUtil.deduceDataId(nacos, ApplicationContextHolder.getEnvironment(), configFileType);
        String group = NacosUtil.getGroup(nacos, "DEFAULT_GROUP");

        try {
            configService.addListener(dataId, group, this);
            log.info("DynamicLog refresher, add listener success, dataId: {}, group: {}", dataId, group);
        } catch (NacosException e) {
            log.error("DynamicLog refresher, add listener error, dataId: {}, group: {}", dataId, group, e);
        }
    }

    @Override
    public Executor getExecutor() {
        return null;
    }

    @Override
    public void receiveConfigInfo(String content) {
        refresh(content, configFileType);
    }

}
