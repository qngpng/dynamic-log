package dynamic.log.cloud.nacos.autoconfigure;

import dynamic.log.cloud.nacos.refresh.CloudNacosRefresher;
import dynamic.log.core.DLogBaseBeanConfiguration;
import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.cloud.nacos.NacosConfigProperties;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动配置
 *
 * @author qp
 * @date 2023/4/15 11:09
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(NacosConfigProperties.class)
@ImportAutoConfiguration(DLogBaseBeanConfiguration.class)
public class DLogAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(CloudNacosRefresher.class)
    @ConditionalOnClass(NacosConfigManager.class)
    public CloudNacosRefresher cloudNacosRefresher() {
        return new CloudNacosRefresher();
    }

}
