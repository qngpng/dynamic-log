package dynamic.log.cloudexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author qp
 * @date 2023/6/5 22:25
 */
@EnableDiscoveryClient
@SpringBootApplication
public class CloudExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudExampleApplication.class, args);
    }
}
