package dynamic.log.cloudexample.controller.package1;

import dynamic.log.core.common.env.DLogProperties;
import dynamic.log.core.common.env.SimpleLogProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author qp
 * @date 2023/4/14 14:36
 */
@Slf4j
@RequestMapping("/test1")
@RestController
public class TestController {

    @Resource
    protected DLogProperties dLogProperties;

    @GetMapping("/log")
    public List<SimpleLogProperties> log() {
        log.debug("debug.....");
        log.info("info.....");
        log.warn("warn.....");
        log.error("error.....");
        return dLogProperties.getDLog();
    }


    /**
     * tomcat下解决异步获取HttpServletRequest异常
     * <p>
     * 必须使用传参形式
     */
    @GetMapping("/get")
    public String get(int a, HttpServletRequest request, HttpServletResponse response) {
        AsyncContext asyncContext = request.startAsync(request, response);
        log.info("{} ----> {}", request.getServletPath(), request.getParameter("a"));
        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(3);
            }
            catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            log.info("{} ----> {}", request.getServletPath(), request.getParameter("a"));
            asyncContext.complete();
        }).start();
        return "success";
    }

}
