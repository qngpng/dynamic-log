package dynamic.log.dynamiclogexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class DynamicLogExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DynamicLogExampleApplication.class, args);
    }

}
