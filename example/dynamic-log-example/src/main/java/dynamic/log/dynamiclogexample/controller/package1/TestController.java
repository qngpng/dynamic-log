package dynamic.log.dynamiclogexample.controller.package1;

import dynamic.log.core.common.env.DLogProperties;
import dynamic.log.core.common.env.SimpleLogProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author qp
 * @date 2023/4/14 14:36
 */
@Slf4j
@RequestMapping("/test1")
@RestController
public class TestController {

    @Resource
    protected DLogProperties dLogProperties;

    /**
     * 日志
     *
     * @return {@link List}<{@link SimpleLogProperties}>
     */
    @GetMapping("/log")
    public List<SimpleLogProperties> log() {
        log.debug("debug.....");
        log.info("info.....");
        log.warn("warn.....");
        log.error("error.....");
        return dLogProperties.getDLog();
    }
}
