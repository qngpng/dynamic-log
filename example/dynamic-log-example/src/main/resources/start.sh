#!/bin/bash
#export JAVA_HOME=/opt/soft/java/jdk1.8.0_201
#export JRE=/opt/soft/java/jdk1.8.0_201/jre
#export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib:$CLASSPATH
#export PATH=$PATH:$JAVA_HOME/bin/:$JRE/bin

source /etc/profile

MAX_DUMP=256m
MIN_DUMP=256m

# 计算合适的最大、最小堆

JAVA_OPTS="
-Xmx$MAX_DUMP
-Xms$MIN_DUMP 
-XX:HeapDumpPath=/tmp/dump.hprof 
-XX:+HeapDumpOnOutOfMemoryError"
CATALINA_OPTS="
-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5015"

# 检查是否启动 Debug 模式，如果是则加入 Debug 参数
#if [ "$1" = "debug" ];
#then
#  JAVA_OPTS="$JAVA_OPTS $CATALINA_OPTS"
#fi

# 获取当前脚本的绝对路径
APP_HOME=$(
  cd "$(dirname "$0")" || exit
  pwd
)

JAR_NAME=boot-example-2023.0.2.jar
LOG_NAME=logs.log

# 备份函数
backup() {
  echo "Backing $JAR_NAME..."

  cp_dir=$(which cp)

  echo "cp in $cp_dir"
  echo "APP_HOME in $APP_HOME"

  backup_dir=$APP_HOME/backup
  [ ! -d "$backup_dir" ] && mkdir -p "$backup_dir"
  # 复制jar包到备份目录
  $cp_dir -f "$APP_HOME/$JAR_NAME" "$backup_dir/$JAR_NAME.back"

  echo "Backed."
}

#关闭项目
stop() {
  echo "Stopping $JAR_NAME..."

  WAIT_TIME=3
  pid_2=$(pgrep -f "$APP_HOME/$JAR_NAME")
  # shellcheck disable=SC2009
  pid_3=$(ps -ef | grep "$APP_HOME/$JAR_NAME" | grep -v grep | awk '{print $3}')
  echo "旧应用进程id： $pid_2 $pid_3"

  if [[ -n "$pid_2" && "$pid_3" -gt 1 ]]; then
    echo "尝试关闭子进程：$pid_2"
    kill -15 "$pid_2"

    # 等待应用进程结束
    count=0
    while [ -e /proc/"$pid_2" ]; do
      if [ "$count" -gt "$WAIT_TIME" ]; then
        echo "Child process did not stop within $WAIT_TIME seconds. Try to closing parent process: $pid_3."
        # 关闭父进程
        p_count=0
        pkill -P "$pid_3"
        while [ -e /proc/"$pid_3" ]; do
          if [ "$p_count" -gt "$WAIT_TIME" ]; then
            echo "ERROR: Parent process did not stop within $WAIT_TIME seconds. Please manually resolve."
            break
          fi

          sleep 1
          ((p_count++))
        done

        break
      fi

      sleep 1
      ((count++))
    done
    echo "Stopped."

  elif [[ -n "$pid_2" && "$pid_3" -eq 1 ]]; then
    echo "关闭子进程：$pid_2"
    kill -15 "$pid_2"

    # 等待应用进程结束
    count=0
    while [ -e /proc/"$pid_2" ]; do
      if [ "$count" -gt "$WAIT_TIME" ]; then
        echo "ERROR: Child process did not stop within $WAIT_TIME seconds. Please manually resolve."
        break
      fi

      sleep 1
      ((count++))
    done
    echo "Stopped."

  else
    echo "$JAR_NAME is not running."
  fi
}

#启动项目
start() {

  echo "Starting $JAR_NAME..."
  # 停止已经启动的应用
  stop
  #which java
  #echo $JAVA_HOME
  JAVA_OPTS="$JAVA_OPTS $CATALINA_OPTS"

  echo "java $JAVA_OPTS -jar $APP_HOME/$JAR_NAME > $APP_HOME/$LOG_NAME 2>&1 &"

  # shellcheck disable=SC2086
  nohup java $JAVA_OPTS -jar $APP_HOME/$JAR_NAME >$APP_HOME/$LOG_NAME 2>&1 &

  #nohup java "${JAVA_OPTS[@]}" -jar "${APP_HOME}/${JAR_NAME}" > "${APP_HOME}/${LOG_NAME}" 2>&1 &
  #sleep 5

  pid=$(pgrep -f "$APP_HOME/$JAR_NAME")
  if [ -n "$pid" ]; then
    echo "Start success!!!  $pid"
  fi

}

CMD=$1

case "$CMD" in

start)
  start
  exit 0
  ;;
stop)
  stop
  exit 0
  ;;

backup)
  backup
  exit 0
  ;;

*)
  start
  exit 1
  ;;
esac
